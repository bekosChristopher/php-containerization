FROM ubuntu:20.04
RUN apt update \
     && apt install software-properties-common -y \
     && add-apt-repository ppa:ondrej/php -y \
     && apt install php -y
WORKDIR /dummy
COPY demo.php /dummy/demo.php